# Outline

## Linux Commands - WorkBook

* Introduction [link](0_intro.md)
* Linux CLI Environment [link](1_cli.md)
* General Purpose Commands [link](2_general.md)
* Handling Files & Directories [link](3_files.md)
* Filter Commands [link](4_filter.md)
* Shell Features [link](5_shell.md)
* Customizing Environment [link](6_env.md)
* File System Utils [link](7_fs.md)
